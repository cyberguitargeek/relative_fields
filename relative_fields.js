/**
 * Relative Fields 
 *
 * A plugin that handles the relative updating of two or more input fields.  
 * Author:  Chad Kelley, csharpkelley@gmail.com
 *
 * relative_fields.js - This file is part of the Relative Update project.  
 *
 *  The MIT License (MIT)
 * 
 * Copyright (c) 2014 Chad Kelley, csharpkelley@gmail.com, chadguitar@live.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

(function($) {

	$.fn.relativeFields = function(opts) {
		var defaults = {
			otherFields: [],
			borderColor: 'orange',
			useSpinner: true 
		}; 
		var settings = $.extend(defaults, opts); 
		
		// Update the other fields in a relative way when a keyup event is triggered. 
		var updateOtherFields = function(el) {
			var oldVal = $(el).data('oldVal');
			var newVal = $(el).val();
			var delta = oldVal - newVal; 
			for (var n = 0; n < settings.otherFields.length; n++) {
				var fld = settings.otherFields[n]; 
				var fldVal = $(fld).val();
				$(fld).val(fldVal - delta); 
			}
			$(el).data('oldVal', newVal); 
		}

		// The main driver function. 
		$(this).each(function() {
			// Does the jQuery UI spinner widget exist? 
			if (!$(this).spinner) {
				settings.useSpinner = false; 
			}
			var inputEl = this; 
			// Set the styling based on if this uses a jQuery UI spinner widget or not.  
			if (settings.useSpinner === true) {
				$(this).css({
					"border": "none"
				});
			} else {
				$(this).css({
					"border": "solid " + settings.borderColor + " 1px"
				});				
			}

			$(this).data("oldVal", parseInt($(this).val())); 
			$(this).on("keyup", function() {
				updateOtherFields(this);  
			}); 
			if (settings.useSpinner === true) {
				$(this).spinner({
					stop: function(event, ui) {
						updateOtherFields(inputEl);  
					}
				});				
			}
			
		});
		return this; 
	}
	
})(jQuery)

